//
//  WeatherViewModel.swift
//  YourWeather
//
//  Created by Farakh Salman Bhatti on 27/03/2022.
//

import Foundation
import UIKit.UIImage
import CoreLocation


public class WeatherViewModel {
    var currentLocationName = "" {
        didSet {
            fetchWeatherForLocation(currentLocationName)
            fetchWeatherForNextWeek(currentLocationName)
        }
    }
    var isNetworkAvailable = Box(false)
    var isLocationServicesAllowed = Box(false)
    var currentWeatherData: Box<WeatherData?> = Box(nil)
    var forecastWeatherData: Box<WeatherForecastData?> = Box(nil)
    var locationManager:CLLocationManager?
    private let dateFormatter: DateFormatter = {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "EEEE"
      return dateFormatter
    }()
    init(locManager:CLLocationManager) {
        locationManager = locManager
        locationManager?.requestWhenInUseAuthorization()
    }
    
    private func fetchWeatherForLocation(_ location: String) {
        WeatherAPIService.weatherDataForLocation(curretnLocationName: location)
        { [weak self] (weatherData, error) in
            
            self?.currentWeatherData.value = weatherData
        }
    }
    private func fetchWeatherForNextWeek(_ location: String) {
        WeatherAPIService.weatherDataForNextWeek(curretnLocationName: location)
        { [weak self] (weatherData, error) in
            
            self?.forecastWeatherData.value = weatherData
        }
    }
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
        }
    }
    func getWeekDayNameFromDate(timeStamp:Int)-> String{
        var weekDay = ""
        if timeStamp == 0{
            return "Today"
        }else{
            let tempTimeStamp  = Double(timeStamp)
            let date = Date(timeIntervalSince1970: tempTimeStamp)
            weekDay = dateFormatter.string(from: date)
            return weekDay
        }
    }
    func checkConnectivity(){
        if Reachability.isConnectedToNetwork() {
            isNetworkAvailable.value = true
        } else {
            isNetworkAvailable.value = false
        }
    }
    func updateCurrentLocation(){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                isLocationServicesAllowed.value = false
                //showLocationPermissionsErrorPopup()
                 print("")
            case .authorizedAlways, .authorizedWhenInUse:
                isLocationServicesAllowed.value = true
                //weatherViewModel.locationManager?.delegate = self
                locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager?.startUpdatingLocation()
            @unknown default:
                break
            }
            
        }
    }
}
extension WeatherView:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocation = manager.location else { return }
        weatherViewModel.fetchCityAndCountry(from: location) { [self] city, country, error in
            guard let city = city, error == nil else { return }
            if  weatherViewModel.currentLocationName == ""{
                weatherViewModel.currentLocationName = city
            }
        }
    }
    
}
extension WeatherView: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.weatherViewModel.forecastWeatherData.value?.list?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.weatherCellIdentifier) as! WeatherForecastCell
        let weatherData = self.weatherViewModel.forecastWeatherData.value?.list?[indexPath.row]
        
        cell.weekDay.text = self.weatherViewModel.getWeekDayNameFromDate(timeStamp:weatherData?.dt ?? 0)
        cell.tempMin.text = "\(Int(weatherData?.temp?.min ?? 0))"
        cell.tempMax.text = "\(Int(weatherData?.temp?.max ?? 0))"
        // set icon
        let iconName = weatherData?.weather?.first?.icon ?? ""
        if iconName != "" {
            let url = URL(string: WeatherAPIService.iconPath + iconName + WeatherAPIService.imageSizeAndType)
            cell.weatherImage.kf.setImage(with: url)
        }
        return  cell
    }
    
}




