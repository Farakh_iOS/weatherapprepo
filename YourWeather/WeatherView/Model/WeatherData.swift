//
//  WeatherData.swift
//  YourWeather
//
//  Created by Farakh Salman Bhatti on 27/03/2022.
//

import Foundation
// MARK: - WeatherData
struct WeatherData: Codable {
    let coord: Coord?
    let weather: [Weather]?
    let base: String?
    let main: Main?
    let visibility: Int?
    let wind: Wind?
    let clouds: Clouds?
    let dt: Int?
    let sys: Sys?
    let timezone, id: Int?
    let name: String?
    let cod: Int?
}

// MARK: - Clouds
struct Clouds: Codable {
    let all: Int?
}

// MARK: - Coord
struct Coord: Codable {
    let lon, lat: Double?
}

// MARK: - Main
struct Main: Codable {
    let temp, feelsLike, tempMin, tempMax: Double?
    let pressure, humidity: Int?

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure, humidity
    }
}

// MARK: - Sys
struct Sys: Codable {
    let type, id: Int?
    let country: String?
    let sunrise, sunset: Int?
}

// MARK: - Weather
struct Weather: Codable {
    let id: Int?
    let main, weatherDescription, icon: String?

    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription = "description"
        case icon
    }
}
// MARK: - Wind
struct Wind: Codable {
    let speed: Double?
    let deg: Int?
    let gust: Double?
}

//struct WeatherData: Decodable {
//
//  private static let dateFormatter: DateFormatter = {
//    var formatter = DateFormatter()
//    formatter.dateFormat = "yyyy-MM-dd"
//    return formatter
//  }()
//
//  //MARK: - JSON Decodables
//
//  let observation: [Observation]
//
//  struct Observation: Decodable {
//    let temp: Double
//    let datetime: String
//
//    let weather: Weather
//
//    struct Weather: Decodable {
//      let icon: String
//      let description: String
//    }
//  }
//
//  enum CodingKeys: String, CodingKey {
//    case observation = "data"
//  }
//
//  //MARK: - Convenience getters
//
//  var currentTemp: Double {
//    observation[0].temp
//  }
//
//  var iconName: String {
//    observation[0].weather.icon
//  }
//
//  var description: String {
//    observation[0].weather.description
//  }
//
//  var date: Date {
//    //strip off the time
//    let dateString = String(observation[0].datetime.prefix(10))
//    //use current date if unable to parse
//    return Self.dateFormatter.date(from: dateString) ?? Date()
//  }
//
//}
