//
//  WeatherForecastCell.swift
//  YourWeather
//
//  Created by Farakh Salman Bhatti on 27/03/2022.
//

import UIKit

class WeatherForecastCell: UITableViewCell {
    @IBOutlet weak var weekDay: UILabel!
    @IBOutlet weak var tempMax: UILabel!
    @IBOutlet weak var tempMin: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
