//
//  WeatherView.swift
//  YourWeather
//
//  Created by Farakh Salman Bhatti on 27/03/2022.
//

import UIKit
import CoreLocation
import Kingfisher

class WeatherView: UIViewController {
    // Outlets
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var weatherMain: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var tempMax: UILabel!
    @IBOutlet weak var tempMin: UILabel!
    @IBOutlet weak var feelsLike: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var weakDay: UILabel!
    @IBOutlet weak var mainIconImage: UIImageView!
    @IBOutlet weak var weatherForecastTableView: UITableView!
    
    // ViewMdel
    let weatherViewModel = WeatherViewModel(locManager: CLLocationManager())
    
    // Constants
    let weatherCellIdentifier = "WeatherForecastCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Curretn Weather Details
        weatherViewModel.currentWeatherData.bind { [weak self] data in
            self?.locationName.text = data?.name ?? "Searching....."
            self?.weatherMain.text = data?.weather?.first?.main
            self?.temperature.text =  NSString(format:"\(Int(data?.main?.temp ?? 00))%@" as NSString, "\u{00B0}") as String
            self?.tempMin.text = "Min: \(Int(data?.main?.tempMin ?? 0))"
            self?.tempMax.text = "Max: \(Int(data?.main?.tempMax ?? 0))"
            self?.humidity.text = "Humidity: \(Int(data?.main?.humidity ?? 0))%"
            self?.feelsLike.text = "Feels like: \(Int(data?.main?.temp ?? 0))"
            self?.weakDay.text = self?.weatherViewModel.getWeekDayNameFromDate(timeStamp: data?.dt ?? 0)
            // set icon
            let iconName = data?.weather?.first?.icon ?? ""
            if iconName != "" {
                let url = URL(string: WeatherAPIService.iconPath + iconName + WeatherAPIService.imageSizeAndType)
                self?.mainIconImage.kf.setImage(with: url)
            }
        }
        // Forcasted Weather Details
        weatherViewModel.forecastWeatherData.bind{ [weak self] data in
            self?.weatherForecastTableView.reloadData()
        }
        weatherViewModel.isNetworkAvailable.bind{ [weak self] connected in
            if connected {
                self?.weatherViewModel.updateCurrentLocation()
            }else{
                self?.showNetworkErroPopup()
            }
        }
        weatherViewModel.isLocationServicesAllowed.bind{ [weak self] isAllowed in
            if !isAllowed {
                self?.showLocationPermissionsErrorPopup()
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    func setupView(){
        registerXIB()
        weatherForecastTableView.delegate = self
        weatherForecastTableView.dataSource = self
        weatherForecastTableView.tableFooterView = UIView(frame: .zero)
        weatherViewModel.locationManager?.delegate = self
        weatherViewModel.checkConnectivity()
    }
    func registerXIB(){
        weatherForecastTableView.register(UINib.init(nibName: weatherCellIdentifier, bundle: Bundle.main), forCellReuseIdentifier: weatherCellIdentifier)
    }
    func showNetworkErroPopup() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Network Error", message: "No connectivity. Please check your internet connection.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                    case .default:
                    print("default")
                    default:
                    fatalError()
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func showLocationPermissionsErrorPopup() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Authorization Error", message: "Please go to settings and allow an app to use location services in order to access current location.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                    case .default:
                    print("default")
                    default:
                    fatalError()
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
