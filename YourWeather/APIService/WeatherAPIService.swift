//
//  WeatherAPIService.swift
//  YourWeather
//
//  Created by Farakh Salman Bhatti on 27/03/2022.
//

import Foundation
enum OpenWeatherMapError: Error {
    case invalidResponse
    case noData
    case failedRequest
    case invalidData
}

class WeatherAPIService {
    typealias WeatherDataCompletion = (WeatherData?, OpenWeatherMapError?) -> ()
    typealias WeatherForecastDataCompletion = (WeatherForecastData?, OpenWeatherMapError?) -> ()
    
    private static let apiKey = "1d7143982emshb5ed42dd60e8e98p1157a7jsn286ebe9960d0"
    private static let host = "community-open-weather-map.p.rapidapi.com"
    private static let currentWeather = "/weather"
    private static let forecast = "/forecast/daily"
    static let iconPath = "https://openweathermap.org/img/wn/"
    static let imageSizeAndType =  "@2x.png"
    static func weatherDataForLocation(curretnLocationName: String,completion: @escaping WeatherDataCompletion){
        let headers = [
            "X-RapidAPI-Host": host,
            "X-RapidAPI-Key": apiKey
        ]

        let request = NSMutableURLRequest(url: NSURL(string: "https://\(host)\(currentWeather)?q=\(curretnLocationName)&units=metric")! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                                timeoutInterval: 20.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async {
                if (error != nil) {
                    print(error!)
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    print(httpResponse!)
                    do {
                      let decoder = JSONDecoder()
                        let weatherData: WeatherData = try decoder.decode(WeatherData.self, from: data!)
                      completion(weatherData, nil)
                    } catch {
                      print("Unable to decode OpenWeatherService response: \(error.localizedDescription)")
                      completion(nil, .invalidData)
                    }
                }
            }
          
        })

        dataTask.resume()
    }
    static func weatherDataForNextWeek(curretnLocationName: String,completion: @escaping WeatherForecastDataCompletion){
        let headers = [
            "X-RapidAPI-Host": host,
            "X-RapidAPI-Key": apiKey
        ]
        let request = NSMutableURLRequest(url: NSURL(string: "https://\(host)\(forecast)?q=\(curretnLocationName)&cnt=8&units=metric")! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                                timeoutInterval: 20.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async {
                if (error != nil) {
                    print(error!)
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    print(httpResponse!)
                    do {
                      let decoder = JSONDecoder()
                        var weatherData: WeatherForecastData = try decoder.decode(WeatherForecastData.self, from: data!)
                        weatherData.list?.removeFirst()
                      completion(weatherData, nil)
                    } catch {
                      print("Unable to decode OpenWeatherService response: \(error.localizedDescription)")
                      completion(nil, .invalidData)
                    }
                }
            }
          
        })

        dataTask.resume()
    }
}
